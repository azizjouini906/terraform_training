variable "tenant_id"{
    default =""
}
variable "client_secret" {
  default = ""
}

variable "client_id" {
  default = ""
}

variable "subscription_id" {
  default = ""
}