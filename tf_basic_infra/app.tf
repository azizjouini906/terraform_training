resource "azurerm_service_plan" "shopware-asp" {
  name                = join("", [local.name_prefix, "sp", "1"])
  location            = local.resource_group_location
  resource_group_name = local.resource_group_name
  os_type             = "Linux"
  sku_name            = local.plan_sku_web_name

  
}


resource "azurerm_linux_web_app" "shopware-webapp" {
  name                = join("", [local.name_prefix, "wa", "1", "skx047"])
  location            = azurerm_resource_group.SHOPWARE_RSG.location
  resource_group_name = azurerm_resource_group.SHOPWARE_RSG.name
  service_plan_id     = azurerm_service_plan.shopware-asp.id
  https_only          = true

  identity {
    type = "SystemAssigned"
  }

  app_settings = {
    DOCKER_CUSTOM_IMAGE_NAME            = ""
    DOCKER_ENABLE_CI                    = "true"
    DOCKER_REGISTRY_SERVER_URL          = ""
    WEBSITES_ENABLE_APP_SERVICE_STORAGE = "false"
    DOCKER_REGISTRY_SERVER_USERNAME     = data.azurerm_key_vault_secret.cr-user.value
    DOCKER_REGISTRY_SERVER_PASSWORD     = data.azurerm_key_vault_secret.cr-pw.value
  }

  site_config {
    app_command_line       = ""
    ftps_state             = ""
    http2_enabled          = true
    always_on              = true
    vnet_route_all_enabled = true
    application_stack {
      docker_image     = ""
      docker_image_tag = "latest"
    }
  }

  tags = merge(
    module.tags.full_tags,
    {
      "security"   = "{critical:1, checked:0, checkedBy: none}",
      "monitoring" = "disabled"
    }
  )

  virtual_network_subnet_id = azurerm_subnet.webapp.id

  lifecycle {
    ignore_changes = [
      app_settings
    ]
  }
}